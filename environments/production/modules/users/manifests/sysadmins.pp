# unixadmin.pp
#
# Realize the members of the Unix team and include any contractors
# Class: users::unixadmins
#
# This class installs our default set of users in our servers
#
# Parameters:
#
#
#
# Actions:
#   Install the default set of administrative users: [John, Mike, Kathy, etc]
class users::sysadmins inherits users {

  ##### System Security Scan Account - Nessus ######
  user { 'gollum':
    ensure              => present,
    comment             => 'Nessus Security Scan Account',
    uid                 => 550,
    home                => '/home/gollum',
    managehome          => true,
    password_min_age    => '0',
    password_max_age    => '99999',
    password            => hiera('gollum_password'),
  }

}
