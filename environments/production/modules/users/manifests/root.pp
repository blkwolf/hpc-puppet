# unixadmin.pp
#
# Realize the members of the Unix team and include any contractors
# Class: users::unixadmins
#
# This class installs our default set of users in our servers
#
# Parameters:
#
#
#
# Actions:
#   Install the default set of administrative users:
class users::root {

	# Setup root user account
  user { 'root':
    ensure       => present, # present, absent
    comment      => 'root user',
    managehome   => true,
    home	       => '/root',
    password     => hiera('root_password'),
  }

  ssh_authorized_key { "root_puppet":
    ensure => "present",
    type => "ssh-rsa",
    key => hiera('root_mgmtp110_ssh'),
    user => "root",
  }

  file { "/root/.bashrc":
    owner   => "root",
    group   => "root",
    mode    => 644,
    content => template("users/bashrc.erb"),
    require => User["root"],
  }

  file { "/root/.bashrc.custom":
    owner   => "root",
    group   => "root",
    require => User["root"],
    content => template("users/bashrc-root.erb"),
  }

}
