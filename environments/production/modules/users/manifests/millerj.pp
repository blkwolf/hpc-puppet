# Module: users
#
# Class:  users::millerj
#
#
# Parameters:
#
# Actions:  Configuration files for user millerj
#
# Requires:
#
class users::millerj {

  user { 'millerj':
    ensure            => present,
    comment           => hiera('millerj_user'),
    uid               => 700,
    groups            => [ 'wheel' ],
    managehome        => true,
    password          => hiera('millerj_password'),
  }

  ssh_authorized_key { "millerj_key1":
    ensure => "present",
    type => "ssh-rsa",
    key => hiera('millerj_s2046_ssh'),
    user => "millerj",
  }

  ssh_authorized_key { "millerj_key2":
    ensure => "present",
    type => "ssh-dss",
    key => hiera('millerj_home_ssh'),
    user => "millerj",
  }

}
