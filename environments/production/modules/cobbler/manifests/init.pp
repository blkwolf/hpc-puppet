# Module: cobbler
#
# Class:  cobbler
#
#
# Parameters:
#
# Actions:  Install and configure cobbler  automated linux deployment system
#
# Requires:
#
class cobbler {

include cobbler::install, cobbler::config, cobbler::service

}
