# Module: cobbler
#
# Class:  cobbler::install
#
#
# Parameters:
#
# Actions:  Install cobbler and related dependencies
#
# Requires:
#
class cobbler::install {

  $cobblerlist = ["cobbler", "cobbler-web", "koan", "pykickstart"]

  package{$cobblerlist:
    ensure => present,
  }


}
