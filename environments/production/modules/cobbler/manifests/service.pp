# Module: cobbler
#
# Class:  cobbler::service
#
#
# Parameters:
#
# Actions:  Sets cobblerd to run on system start, starts service if not running
#
# Requires: Class["cobbler::config"]
#
class cobbler::service {

  service {'cobblerd':
    enable    =>  true,
    ensure    =>  running,
    require   =>  Class["cobbler::config"],
    notify    =>  Exec["cobblersync"],
  }

  # Install the cobbler bootloaders
  exec {'bootloaders':
    command => "/usr/bin/cobbler get-loaders",
    creates => "/var/lib/cobbler/loaders/grub-x86_64.efi",
    notify  => Service["cobblerd"],
  }

  # now that intitial configuration is complete,  sync cobbler after any config changes
  exec {'cobblersync':
    command     => "/usr/bin/cobbler sync",
    refreshonly => true,
  }

}
