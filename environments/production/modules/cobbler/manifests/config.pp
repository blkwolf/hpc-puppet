# Module: cobbler
#
# Class:  cobbler::config
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class cobbler::config {

  $root_password      = hiera('root_password')
  $cobbler_server_ip  = hiera('cobbler_server_ip')
  $cobbler_subnet     = hiera('cobbler_subnet')
  $cobbler_netmask    = hiera('cobbler_netmask')
  $cobbler_bootrange  = hiera('cobbler_bootrange')

  # main cobbler settings
  file {"cobbler_settings":
    path    =>  "/etc/cobbler/settings",
    ensure  =>  "file",
    content =>  template("cobbler/settings.erb"),
    notify  =>  Service["cobblerd"],
  }

  # tell cobbler to use either bind + dhcpd,  or dnsmasq
  file {"cobbler-modules":
    path    =>  "/etc/cobbler/modules.conf",
    ensure  =>  "file",
    content =>  template("cobbler/modules.conf.erb"),
    notify  =>  Service["cobblerd"],
  }

  # Template file for dnsmasq configuration
  file {"dnsmasq-template":
    path    =>  "/etc/cobbler/dnsmasq.template",
    ensure  =>  "file",
    content =>  template("cobbler/dnsmasq.template.erb"),
    notify  =>  Service["cobblerd"],
  }

  # Template file for dhcpd configuration
  file {"dhcp-template":
    path    =>  "/etc/cobbler/dhcp.template",
    ensure  =>  "file",
    content =>  template("cobbler/dhcp.template.erb"),
    notify  =>  Service["cobblerd"],
  }

}
