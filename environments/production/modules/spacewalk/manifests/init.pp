# Module: spacewalk
#
# Class:  spacewalk
#
#
# Parameters:
#
# Actions: Installs Fedora Spacewalk for managing Linux systems
#
# Requires:
#
class spacewalk {

   exec {"space_repo":
    command =>  "rpm -Uvh http://yum.spacewalkproject.org/1.8/RHEL/6/x86_64/spacewalk-repo-1.8-4.el6.noarch.rpm",
    creates =>  "/etc/yum.repos.d/spacewalk.repo",
    before  => Package["spacewalk-postgresql"],
  }

  file {"jpackge-repo":
    path  => "/etc/yum.repos.d/jpackage.repo",
    content =>  template("spacewalk/jpackage_repo.erb"),
    before  =>  Package["spacewalk-postgresql"],
  }

  package {"spacewalk-postgresql":
    ensure  => installed,
  }

  package {"spacewalk-setup-embedded-postgresql":
    ensure  => installed,
    require => Package["spacewalk-setup-embedded-postgresql"],
  }
}
