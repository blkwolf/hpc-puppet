# Module: create_repo
#
# Class:  create_repo
#
#
# Parameters:
#
# Actions: Installs createrepo package, builds a local repository and updates database
#
# Requires:
#
class create_repo {

  $repos_path = ["/var/www/html/repos/", "/var/www/html/repos/cluster"]


  package {"createrepo":
    ensure  =>  present,
    before  =>  Exec["build_repo"],
  }


  file {$repos_path:
    ensure  =>  "directory",
    require =>  [ Class["httpd"],
                  Service["httpd"]
                ],
    before  =>  Exec["build_repo"],
  }

  file {"cluster_repo":
    path    =>  "/etc/yum.repos.d/cluster.repo",
    ensure  =>  "file",
    content  =>  template("create_repo/cluster.repo.erb"),
    before  =>  Exec["build_repo"],
    require =>  [ Class["httpd"],
                  Service["httpd"]
                ],
  }


  exec {"build_repo":
    refreshonly =>  true,
    command     =>  "/usr/bin/createrepo /var/www/html/repos/cluster",
    require     =>  File["/var/www/html/repos/cluster"],
  }



}
