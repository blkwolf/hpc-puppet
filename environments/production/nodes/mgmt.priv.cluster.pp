# Puppet master server and file repository for rpms subversion isos etc
# External hostname: akc0ss-p110.afsc.noaa.gov
node 'mgmt.priv.cluster' {

  # notify { "mgmt": message => "NODECLASS=mgmt.priv.cluster" }  # testing

  # default classes modules, also located in node {default: }
  include hostfile, selinux, resolv_conf, iptables::headnode, users::root

  # head node specific classes and modules
  # Initial modules, enable these first when setting up cluster for the first time
  include users, users::sysadmins, puppet, puppet::tagmail, httpd

  # After intial configuration applied via above modules, enable these to convert puppet master to run on apache + passenger
  include httpd::passenger, puppet::passenger


  # redhat and related cluster tools enable after passenger configuration
  include cluster, cluster::manage, create_repo, r_language, r_language::rstudio

  # spacewalk related modules and classes
  # include spacewalk


}
