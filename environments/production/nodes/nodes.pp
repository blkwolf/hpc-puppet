# nodes.pp
#
# Configures the default node configuration
# Note:  "ONLY" applies to nodes that are not defined elsewhere
# http://docs.puppetlabs.com/puppet/3/reference/lang_node_definitions.html#the-default-node
node default {
  # notify { "nodef": message => "NODECLASS=default" }   # testing

  # Include default modules
  include hostfile, selinux, resolv_conf, users::root

  case $fqdn {
    default: {
      include iptables::compute
    }
    "mgmt.priv.cluster": {
      include iptables::headnode
    }
  }

}
