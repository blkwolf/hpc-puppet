# site.pp

#  notify { "env": message => "ENVIRONMENT=production" }  # testing

import "nodes/*.pp"


# Set system paths for puppet exec command
  Exec {
    path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
  }
