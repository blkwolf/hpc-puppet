# Module: selinux
#
# Class:  selinux::service
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class selinux::service {
  $require = Class["selinux::config"]

  # Create script file to run selinux commands
  file {"se-afsc.sh":
    path    => "/usr/local/sbin/se-afsc.sh",
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => 740,
    content => template("selinux/se-afsc.sh.erb"),
    require => File["sudonrpe.te"],
  }

  # Execute script file
  exec {"senrpe":
    cwd     => "/etc/selinux/puppet",
    command => "/usr/local/sbin/se-afsc.sh",
    creates => "/etc/selinux/puppet/sudonrpe.mod",
    require => File["se-afsc.sh"],
  }

}
