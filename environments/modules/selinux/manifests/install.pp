# Module: selinux
#
# Class:  selinux::install
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class selinux::install {

  $packagelist = ["policycoreutils", "policycoreutils-python", "setools-console"]

  package {$packagelist:
    ensure => present,
  }

}
