# Module: selinux
#
# Class:  selinux::config
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class selinux::config {

  $require = Class["selinux::install"]

  # master selinux config file
  file {"seconfig":
    path    => "/etc/selinux/config",
    ensure  => file,
    content => template("selinux/config.erb"),
  } # file


  # Directory for storing puppet managed selinux policies
  file {"sepuppet":
    path    => "/etc/selinux/puppet",
    ensure  => directory,
  }

  ####### Allow nrpe to run sudo commands for nagios monitoring #########
  # Initial nrpe sudo selinux allow rules
  file {"sudonrpe.te":
    path    => "/etc/selinux/puppet/sudonrpe.te",
    ensure  => file,
    content => template("selinux/sudonrpe.te.erb"),
  }

  $notify  = Class["selinux::service"]

}
