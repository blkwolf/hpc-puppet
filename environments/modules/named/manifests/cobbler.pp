# Module: named
#
# Class:  named::cobbler
#
#
# Parameters:
#
# Actions: Installs and configures named/bind daemon specifically for managment via cobbler
#
# Requires:
#
class named::cobbler {

  # Install the DNS and DNSSEC bind packages and related files
  $packagelist = ["bind", "bind-libs", "bind-utils", "bind-chroot"]

  package {
    $packagelist: ensure => installed,
    # before => Class["named::cobbler::config"],
  }

  service {"named":
    enable  =>  true,
    ensure  =>  running,
  }


}
