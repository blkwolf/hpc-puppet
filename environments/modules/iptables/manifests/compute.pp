# Module: iptables::compute
#
# Class:  iptables
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class iptables::compute {

  file {
    "iptables":
    mode => 600, owner => root, group => root,
    ensure => present,
    path => "/etc/sysconfig/iptables",
    content => template("iptables/compute.erb"),
  }

  service { "iptables":
    ensure => running,
    enable => true,
    hasrestart => false,
    restart => "iptables-restore < /etc/sysconfig/iptables",
    hasstatus => true,
    subscribe => File["iptables"],
  } # service

}
