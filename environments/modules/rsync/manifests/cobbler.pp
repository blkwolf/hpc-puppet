# Module: rsync
#
# Class:  rsync::cobbler
#
#
# Parameters:
#
# Actions: Installs rsync and runs in xinetd service mode for cobbler
#
# Requires:
#
class rsync::cobbler {

  package {"rsync":
    ensure  =>  present
  }

  file {"/etc/xinetd.d/rsync":
    ensure  =>  file,
    content =>  template("rsync/rsync-cobbler.erb")
  }

}
