# Module: rsync
#
# Class:  rsync
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class rsync {

  package {"rsync":
    ensure  =>  present
  }

}
