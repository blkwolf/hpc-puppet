# Module: dhcpd
#
# Class:  dhcpd::install
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class dhcpd::install {

  package {"dhcp":
    ensure  =>  present,
  }

}
