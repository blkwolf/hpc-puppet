# Module: dhcpd
#
# Class:  dhcpd::service
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class dhcpd::service {

   service {"dhcpd":
    enable  =>  true,
    ensure  =>  running,
  }

}
