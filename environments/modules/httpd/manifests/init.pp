# Module: httpd 
#                     
# Class:  httpd 
#                     
# Install Apache webserver
#                    
# Parameters:         
#                     
# Actions:            
#                     
# Requires:           
#                     
class httpd {

  package { httpd:
    ensure => "present", 
  }

  package { "mod_ssl":
    ensure => "present",
    require => Package["httpd"], 
  }  
  
}  
