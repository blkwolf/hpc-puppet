# Module: httpd
#
# Class:  httpd::passenger
#
# Install apache webserver with ruby Passenger + Rack
#
# Parameters:
#
# Actions:
#
# Requires:
#
class httpd::passenger inherits httpd {

  $passengerlist = ["httpd-devel", "rubygem-rack", "mod_passenger", "rubygem-passenger"]

  package { $passengerlist:
    ensure  =>  present,
  }

  service { 'httpd':
    enable    => true,
    ensure    => running,
    hasstatus => true,
    before    => Service["puppetmaster"],
  }

  #Configure apache httpd server for passenger and puppet hosting
  file { "/etc/httpd/conf.d/10_passenger.conf":
    content => template("httpd/10_passenger-conf.erb"),
    notify  => Service[httpd],
    # before => Class["puppet::passenger"],
    before => Service["httpd"],
  }

  file { "/etc/httpd/conf.d/20_puppetmaster.conf":
    content => template("httpd/20_puppetmaster-conf.erb"),
    notify  => Service[httpd],
    # before => Class["puppet::passenger"],
    before => Service["httpd"],
  }

}

