# Module: hostfile
#
# Class: hostfile
#
# Configure default /etc/hosts file for Linux systems
#
# Parameters:
#
# Actions:
#
# Requires:
#
class hostfile {

  $ext_net = hiera('hosts_external')

  file { "/etc/hosts":
    ensure  => file,
    mode    => 664,
    content => template("hostfile/hostfile.erb"),
  }

}
