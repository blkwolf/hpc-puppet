class postgresql::server($pgsqlpath='/var/lib/pgsql/data') {

  class { 'postgresql::client':}

  Class['postgresql::server'] -> Class['postgresql::client']


  package { "postgresql-server":
    ensure => present,
  }

  File {
    owner => 'postgres',
    group => 'postgres',
  }

  file { 'pg_hba.conf':
    path    => "$pgsqlpath/pg_hba.conf",
    content => template("postgresql/pg_hba.conf.erb"),
    mode    => '0640',
    require => Package[postgresql-server],
  }

  file { 'postgresql.conf':
    path    => "$pgsqlpath/postgresql.conf",
    content => template('postgresql/postgresql.conf.erb'),
    require => Package[postgresql-server],
  }

  service { "postgresql":
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => [Package[postgresql-server],
                  File['pg_hba.conf'],
                  File['postgresql.conf']],
  }
}
