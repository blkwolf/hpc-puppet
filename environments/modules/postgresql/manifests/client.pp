class postgresql::client {
  package { "postgresql":
    ensure => present,
  }
}
