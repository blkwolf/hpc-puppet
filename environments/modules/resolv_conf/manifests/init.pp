# Module: resolv_conf
#
# Class:  resolv_conf
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class resolv_conf {

  # configure variables
  $ext_domain  = hiera('ext_domain')
  $ext_nameserver1 =  hiera('ext_nameserver1')
  $ext_nameserver2 =  hiera('ext_nameserver2')

  file { "/etc/resolv.conf":
    owner => root,
    group => root,
    mode => 644,
    content => template("resolv_conf/resolv.conf.erb")
  }

}
