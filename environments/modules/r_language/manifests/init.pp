# Module: r_language
#
# Class:  r_language
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class r_language {

  Package { ensure => "installed" }

  $rpackages = ["R", "R-core", "R-car", "R-devel", "R-lmtest", "R-multcomp", "R-mvtnorm", "R-qtl", "R-nws", "R-systemfit", "R-zoo"]

  package {$rpackages: }

}

