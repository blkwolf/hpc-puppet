# Module: r_language
#
# Class:  r_language::rstudio
#
#
# Parameters:
#
# Actions: Installs Rstudio IDE package for R language development
#
# Requires:
#
class r_language::rstudio {

#   http://download1.rstudio.org/rstudio-0.97.168-x86_64.rpm

  exec { get_rstudio:
    command => "/usr/bin/wget http://download1.rstudio.org/rstudio-0.97.168-x86_64.rpm",
    cwd     => "/var/www/html/repos/cluster",
    creates => "/var/www/html/repos/cluster/rstudio-0.97.168-x86_64.rpm",
    require => Class["r_language"],
    notify  => Exec["build_repo"],
    # before  => Package["rstudio"],
  }

#  package {"rstudio":
#    ensure  =>  "installed"
#  }

}
