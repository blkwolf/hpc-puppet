class puppet::agent {

  # Redundent since puppet needs to actually be installed to run puppet
  package {
    "puppet": ensure => "present",
  }

  # Create cronjob as root that runs puppet agent twice an hour at a random time per host.
  $first_run  = fqdn_rand(30)
  $second_run = fqdn_rand(30) + 30
  cron { "puppet":
    command => "/usr/bin/puppet agent --onetime --no-daemonize --logdest syslog > /dev/null 2>&1",
    user => "root",
    minute  => ["$first_run","$second_run"],
    ensure => present,
  }

  # turn off the puppet agent in init.d since we run it from cron
  service { 'puppet':
    enable => false,
    ensure => stopped,
    hasstatus => true,
  }


}
