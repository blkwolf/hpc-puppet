# configure the /etc/puppet/tagmail.conf file
class puppet::tagmail {

  $admin_email = hiera('admin_email')

  file {"tagmail":
    path    =>  "/etc/puppet/tagmail.conf",
    ensure  =>  file,
    content =>  template("puppet/tagmail.erb")
  }


}
