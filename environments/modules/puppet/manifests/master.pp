# Module: puppet
#
# Class:  puppet::master
#
# Install the Puppet server and setup a cron job to clean out reports over 1 week old.
#
# Parameters:
#
# Actions:
#
# Requires: Package["puppet-server"]
#
class puppet::master inherits puppet{

  package {"puppet-server": ensure => "present"}


	service { 'puppetmaster':
	   enable => true,
	   ensure => running,
	   hasstatus => true,       
	}  

  # Script file to clean out old puppet reports to keep directory size down
  file { "/usr/local/sbin/clean-puppet-reports.sh":
    owner => root,
    group => root,
    mode => 760,
    content => template("puppet/clean-puppet-reports.sh.erb")
  }


  # Setup cronjob to cleanout old puppet client reports every Tuesday at 3:12am
  cron { "cleanpuppetreports":
    command => "/usr/local/sbin/clean-puppet-reports.sh > /dev/null 2>&1",
    user    => "root",    
    minute  => 12,
    hour    => 03,
    weekday => "tuesday",
    ensure  => present,
  }  


#	# Setup cronjob to sync /etc/puppet with mercurial server in 15 minute increments
#    cron { "updatepuppet":
#        command => "hg pull -u /etc/puppet -n 1 > /dev/null 2>&1",
#        user => "root",
#        minute => [02, 17, 32, 47],
#        ensure => present,
#    }	
}
