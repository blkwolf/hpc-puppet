# Module: puppet
#
# Class:  puppet
#
# Installs puppet agent software.
# Configures system to run puppet agent manually every 30 minutes via a random time set in cron.
#
# Note: the puppet master will be configured in either master.pp or passenger.pp depending on
# whether using the built in webbrick or passenger webserver.
# Parameters:
#
# Actions:
#
# Requires:
#
class puppet {

  include puppet::agent

	# Ensure related ruby packages are installed
  $packageList = [ruby-shadow, libselinux-ruby]
  package { $packageList: }

}





