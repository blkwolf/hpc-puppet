# Module: puppet
#
# Class:  puppet::passenger
#
# Configure Puppet Master server to use Apache httpd and Passenger + Rack as the backend for puppet server.
#
# Parameters:
#
# Actions:
#
# Requires: Package["puppet-server"]
# * Package["puppet-server"]
# * Package["mod_passenger"]
# Class[yumrepos::passenger"]
# Class["httpd::apache"]
# Class["httpd::passenger"]
class puppet::passenger inherits puppet {

  package { puppet-server:
    ensure => present,
  }



	# Create directory structure for puppet passenger setup
	file {["/etc/puppet/rack","/etc/puppet/rack/puppetmaster","/etc/puppet/rack/puppetmaster/public","/etc/puppet/rack/puppetmaster/tmp"]:
		ensure => directory,
		owner => root,
	}

 	# Note:  config.ru needs to be set with puppet as owner and group, as apache will drop
 	# down to this lower privledged account as it loads the rack services
  file { "/etc/puppet/rack/puppetmaster/config.ru":
    owner => puppet,
    group => puppet,
    mode => 644,
    content => template("puppet/config_ru.erb"),
    notify  => Service[httpd],
    require => File["/etc/puppet/rack/puppetmaster"]
  }

  # Script file to clean out old puppet reports to keep directory size down
  file { "/usr/local/sbin/clean-puppet-reports.sh":
    owner => root,
    group => root,
    mode => 760,
    content => template("puppet/clean-puppet-reports.sh.erb")
  }


  # Setup cronjob to cleanout old puppet client reports every Tuesday at 3:12am
  cron { "cleanpuppetreports":
    command => "/usr/local/sbin/clean-puppet-reports.sh > /dev/null 2>&1",
    user    => "root",
    minute  => 12,
    hour    => 03,
    weekday => "tuesday",
    ensure  => present,
  }

  # Since we are using apache + passenger for our puppet server, we want to make sure that the puppetmaster service
  # is turned off, and that appache is started.
  service { 'puppetmaster':
    enable => false,
    ensure => stopped,
    hasstatus => true,
   #  notify => Service["httpd"],
  }


} # End of class

