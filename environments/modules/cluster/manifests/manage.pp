# Module: cluster
#
# Class:  cluster::install
#
#
# Parameters:
#
# Actions: Installs luci cluster manager
#
# Requires:
#
class cluster::manage {

  package {"luci":
    ensure  =>  present,
  }

}
