# Module: cluster
#
# Class:  cluster
#
#
# Parameters:
#
# Actions:  Install base cluster / HA packages
#
# Requires:
#
class cluster {

  $storagelist = ["gfs2-utils", "lvm2-cluster"]

  $clusterlist = [
    "cman",
    "ccs",
    "omping",
    "rgmanager",
    "cluster-cim",
    "cluster-glue-libs-devel",
    "cluster-snmp",
    "clusterlib-devel",
    "corosynclib-devel",
    "fence-virtd-checkpoint",
    "foghorn",
    "libesmtp-devel",
    "openaislib-devel",
    "pacemaker",
    "pacemaker-libs-devel",
    "python-repoze-what-quickstart",
  ]



  package { $clusterlist:
    ensure  =>  present,
  }

  package { $storagelist:
    ensure  =>  present,
  }

}
