# Module: yum_repos 
#                     
# Class:  yum_repos 
#                     
#                     
# Parameters:         
#                     
# Actions:            
#                     
# Requires:           
#                     
class yum_repos {   
                      
include yum_repos::install, yum_repos::config, yum_repos::service
                      
}                     
