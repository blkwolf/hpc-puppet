# Module: yum_repos                           
#                                               
# Class:  yum_repos::config                   
#                                               
#                                               
# Parameters:                                   
#                                               
# Actions:                                      
#                                               
# Requires:                                     
#                                               
class yum_repos::config inherits yum_repos {
  
  $require = Class["yum_repos::install"] 
  $notify  = Class["yum_repos::service"] 
  
} 
