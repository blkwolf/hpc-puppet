# Module: yum_repos                           
#                                               
# Class:  yum_repos::service                  
#                                               
#                                               
# Parameters:                                   
#                                               
# Actions:                                      
#                                               
# Requires:                                     
#                                               
class yum_repos::service inherits yum_repos {
  
  $require = Class["yum_repos::config"]    
  
} 
