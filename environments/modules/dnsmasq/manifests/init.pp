# Module: dnsmasq
#
# Class:  dnsmasq
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class dnsmasq {

include dnsmasq::install, dnsmasq::config, dnsmasq::service

  # remove bind and dhcp if they exist
  package {"bind":
    ensure  =>  absent,
  }

  package {"dhcp":
    ensure  =>  absent,
  }

}
