# Module: dnsmasq
#
# Class:  dnsmasq::install
#
#
# Parameters:
#
# Actions:
#
# Requires:
#
class dnsmasq::install {

  package {"dnsmasq":
    ensure  =>  present,
  }

}
