# site.pp  compute environment

  # notify { "env": message => "ENVIRONMENT=compute" }

  node default {
    include hostfile, selinux, resolv_conf, users::root
  }


  node /^compute-\d+$/ {
    include iptables::compute
  }

  Exec {
    path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
  }
